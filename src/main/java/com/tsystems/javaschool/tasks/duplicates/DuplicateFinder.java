package com.tsystems.javaschool.tasks.duplicates;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class DuplicateFinder {

    /**
     * Processes the specified file and puts into another sorted and unique
     * lines each followed by number of occurrences.
     *
     * @param sourceFile file to be processed
     * @param targetFile output file; append if file exist, create if not.
     * @return <code>false</code> if there were any errors, otherwise
     * <code>true</code>
     */
    public boolean process(File sourceFile, File targetFile) {
        Map<String, Integer> map = new TreeMap<>();
        if (sourceFile == null || targetFile == null) throw new IllegalArgumentException();

        try{
            String currentLine;
            BufferedReader br = new BufferedReader(new FileReader(sourceFile));
            while ((currentLine = br.readLine()) != null) {
                if (!map.containsKey(currentLine))
                    map.put(currentLine, 1);
                else map.put(currentLine, map.get(currentLine) + 1);
            }

            FileWriter output = new FileWriter ( targetFile, true );
            for (Map.Entry <String,Integer> temp : map.entrySet()) {
                output.write(temp.getKey() + "[" + temp.getValue() + "]" + "\n");
            }
            output.close();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
