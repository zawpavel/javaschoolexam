package com.tsystems.javaschool.tasks.calculator;


/**
 * Created by pavel on 05.03.17.
 */
public class CalculatorRunner {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        System.out.println(calc.evaluate("2+2*2"));
    }
}
